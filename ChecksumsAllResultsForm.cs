﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Check_Sum_Mary
{
    public partial class ChecksumsAllResultsForm : Form
    {
        public ChecksumsAllResultsForm()
        {
            InitializeComponent();
        }

        public ChecksumsAllResultsForm(string results)
        {
            InitializeComponent();

            txtAllResults.Text = results;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (CSMSaveFileDialog.ShowDialog() == DialogResult.OK)
            {
                //string ext = null;

                //switch (CSMSaveFileDialog.FilterIndex)
                //{
                //    case 1:
                //        ext = ".txt";
                //        break;
                //    case 2:
                //        ext = ".rtf";
                //        break;
                //    case 3:
                //        ext = ".doc";
                //        break;
                //    default:
                //        ext = ".txt";
                //        break;
                //}

                System.IO.StreamWriter streamWrite = new System.IO.StreamWriter(CSMSaveFileDialog.FileName);
                streamWrite.Write(txtAllResults.Text);
                streamWrite.Close();
                streamWrite.Dispose();
            }           
        }
    }
}
