﻿namespace Check_Sum_Mary
{
    partial class CSMMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CSMMainForm));
            this.btnBrowseFiles = new System.Windows.Forms.Button();
            this.lstbxFiles = new System.Windows.Forms.ListBox();
            this.cmbHashImplementation = new System.Windows.Forms.ComboBox();
            this.btnWriteAll = new System.Windows.Forms.Button();
            this.grpbxChecksums = new System.Windows.Forms.GroupBox();
            this.lblRIPEMD160 = new System.Windows.Forms.Label();
            this.txtRIPEMD160 = new System.Windows.Forms.TextBox();
            this.lblSHA512 = new System.Windows.Forms.Label();
            this.lblSHA384 = new System.Windows.Forms.Label();
            this.txtSHA512 = new System.Windows.Forms.TextBox();
            this.txtSHA384 = new System.Windows.Forms.TextBox();
            this.lblSHA1 = new System.Windows.Forms.Label();
            this.txtSHA1 = new System.Windows.Forms.TextBox();
            this.txtSHA256 = new System.Windows.Forms.TextBox();
            this.lblSHA256 = new System.Windows.Forms.Label();
            this.txtMD5 = new System.Windows.Forms.TextBox();
            this.lblMD5 = new System.Windows.Forms.Label();
            this.CSMOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.chkMD5 = new System.Windows.Forms.CheckBox();
            this.chkSHA1 = new System.Windows.Forms.CheckBox();
            this.chkSHA256 = new System.Windows.Forms.CheckBox();
            this.chkSHA384 = new System.Windows.Forms.CheckBox();
            this.chkSHA512 = new System.Windows.Forms.CheckBox();
            this.chkRIPEMD160 = new System.Windows.Forms.CheckBox();
            this.grpbxWriteAll = new System.Windows.Forms.GroupBox();
            this.grpbxChecksums.SuspendLayout();
            this.grpbxWriteAll.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnBrowseFiles
            // 
            this.btnBrowseFiles.Location = new System.Drawing.Point(12, 12);
            this.btnBrowseFiles.Name = "btnBrowseFiles";
            this.btnBrowseFiles.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseFiles.TabIndex = 0;
            this.btnBrowseFiles.Text = "Browse";
            this.btnBrowseFiles.UseVisualStyleBackColor = true;
            this.btnBrowseFiles.Click += new System.EventHandler(this.btnBrowseFiles_Click);
            // 
            // lstbxFiles
            // 
            this.lstbxFiles.FormattingEnabled = true;
            this.lstbxFiles.Location = new System.Drawing.Point(12, 41);
            this.lstbxFiles.Name = "lstbxFiles";
            this.lstbxFiles.Size = new System.Drawing.Size(577, 134);
            this.lstbxFiles.TabIndex = 1;
            this.lstbxFiles.SelectedIndexChanged += new System.EventHandler(this.lstbxFiles_SelectedIndexChanged);
            // 
            // cmbHashImplementation
            // 
            this.cmbHashImplementation.FormattingEnabled = true;
            this.cmbHashImplementation.Items.AddRange(new object[] {
            "Default",
            "CNG (Cryptography Next Generation)",
            "CSP (Cryptographic Service Provider)",
            "Managed"});
            this.cmbHashImplementation.Location = new System.Drawing.Point(367, 19);
            this.cmbHashImplementation.Name = "cmbHashImplementation";
            this.cmbHashImplementation.Size = new System.Drawing.Size(204, 21);
            this.cmbHashImplementation.TabIndex = 2;
            this.cmbHashImplementation.Text = "Default";
            this.cmbHashImplementation.SelectedIndexChanged += new System.EventHandler(this.cmbHashImplementation_SelectedIndexChanged);
            // 
            // btnWriteAll
            // 
            this.btnWriteAll.Location = new System.Drawing.Point(468, 15);
            this.btnWriteAll.Name = "btnWriteAll";
            this.btnWriteAll.Size = new System.Drawing.Size(75, 23);
            this.btnWriteAll.TabIndex = 3;
            this.btnWriteAll.Text = "Write All";
            this.btnWriteAll.UseVisualStyleBackColor = true;
            this.btnWriteAll.Click += new System.EventHandler(this.btnWriteAll_Click);
            // 
            // grpbxChecksums
            // 
            this.grpbxChecksums.Controls.Add(this.lblRIPEMD160);
            this.grpbxChecksums.Controls.Add(this.txtRIPEMD160);
            this.grpbxChecksums.Controls.Add(this.lblSHA512);
            this.grpbxChecksums.Controls.Add(this.lblSHA384);
            this.grpbxChecksums.Controls.Add(this.txtSHA512);
            this.grpbxChecksums.Controls.Add(this.txtSHA384);
            this.grpbxChecksums.Controls.Add(this.lblSHA1);
            this.grpbxChecksums.Controls.Add(this.txtSHA1);
            this.grpbxChecksums.Controls.Add(this.txtSHA256);
            this.grpbxChecksums.Controls.Add(this.lblSHA256);
            this.grpbxChecksums.Controls.Add(this.txtMD5);
            this.grpbxChecksums.Controls.Add(this.lblMD5);
            this.grpbxChecksums.Controls.Add(this.cmbHashImplementation);
            this.grpbxChecksums.Location = new System.Drawing.Point(12, 242);
            this.grpbxChecksums.Name = "grpbxChecksums";
            this.grpbxChecksums.Size = new System.Drawing.Size(577, 257);
            this.grpbxChecksums.TabIndex = 4;
            this.grpbxChecksums.TabStop = false;
            this.grpbxChecksums.Text = "Checksums";
            // 
            // lblRIPEMD160
            // 
            this.lblRIPEMD160.AutoSize = true;
            this.lblRIPEMD160.Location = new System.Drawing.Point(6, 230);
            this.lblRIPEMD160.Name = "lblRIPEMD160";
            this.lblRIPEMD160.Size = new System.Drawing.Size(70, 13);
            this.lblRIPEMD160.TabIndex = 14;
            this.lblRIPEMD160.Text = "RIPEMD160:";
            // 
            // txtRIPEMD160
            // 
            this.txtRIPEMD160.Location = new System.Drawing.Point(82, 227);
            this.txtRIPEMD160.Name = "txtRIPEMD160";
            this.txtRIPEMD160.Size = new System.Drawing.Size(489, 20);
            this.txtRIPEMD160.TabIndex = 13;
            // 
            // lblSHA512
            // 
            this.lblSHA512.AutoSize = true;
            this.lblSHA512.Location = new System.Drawing.Point(23, 184);
            this.lblSHA512.Name = "lblSHA512";
            this.lblSHA512.Size = new System.Drawing.Size(53, 13);
            this.lblSHA512.TabIndex = 12;
            this.lblSHA512.Text = "SHA-512:";
            // 
            // lblSHA384
            // 
            this.lblSHA384.AutoSize = true;
            this.lblSHA384.Location = new System.Drawing.Point(23, 138);
            this.lblSHA384.Name = "lblSHA384";
            this.lblSHA384.Size = new System.Drawing.Size(53, 13);
            this.lblSHA384.TabIndex = 11;
            this.lblSHA384.Text = "SHA-384:";
            // 
            // txtSHA512
            // 
            this.txtSHA512.Location = new System.Drawing.Point(82, 181);
            this.txtSHA512.Multiline = true;
            this.txtSHA512.Name = "txtSHA512";
            this.txtSHA512.Size = new System.Drawing.Size(489, 40);
            this.txtSHA512.TabIndex = 10;
            // 
            // txtSHA384
            // 
            this.txtSHA384.Location = new System.Drawing.Point(82, 135);
            this.txtSHA384.Multiline = true;
            this.txtSHA384.Name = "txtSHA384";
            this.txtSHA384.Size = new System.Drawing.Size(489, 40);
            this.txtSHA384.TabIndex = 9;
            // 
            // lblSHA1
            // 
            this.lblSHA1.AutoSize = true;
            this.lblSHA1.Location = new System.Drawing.Point(35, 86);
            this.lblSHA1.Name = "lblSHA1";
            this.lblSHA1.Size = new System.Drawing.Size(41, 13);
            this.lblSHA1.TabIndex = 8;
            this.lblSHA1.Text = "SHA-1:";
            // 
            // txtSHA1
            // 
            this.txtSHA1.Location = new System.Drawing.Point(82, 83);
            this.txtSHA1.Name = "txtSHA1";
            this.txtSHA1.Size = new System.Drawing.Size(489, 20);
            this.txtSHA1.TabIndex = 7;
            // 
            // txtSHA256
            // 
            this.txtSHA256.Location = new System.Drawing.Point(82, 109);
            this.txtSHA256.Name = "txtSHA256";
            this.txtSHA256.Size = new System.Drawing.Size(489, 20);
            this.txtSHA256.TabIndex = 6;
            // 
            // lblSHA256
            // 
            this.lblSHA256.AutoSize = true;
            this.lblSHA256.Location = new System.Drawing.Point(23, 112);
            this.lblSHA256.Name = "lblSHA256";
            this.lblSHA256.Size = new System.Drawing.Size(53, 13);
            this.lblSHA256.TabIndex = 5;
            this.lblSHA256.Text = "SHA-256:";
            // 
            // txtMD5
            // 
            this.txtMD5.Location = new System.Drawing.Point(82, 57);
            this.txtMD5.Name = "txtMD5";
            this.txtMD5.Size = new System.Drawing.Size(489, 20);
            this.txtMD5.TabIndex = 4;
            // 
            // lblMD5
            // 
            this.lblMD5.AutoSize = true;
            this.lblMD5.Location = new System.Drawing.Point(43, 60);
            this.lblMD5.Name = "lblMD5";
            this.lblMD5.Size = new System.Drawing.Size(33, 13);
            this.lblMD5.TabIndex = 3;
            this.lblMD5.Text = "MD5:";
            // 
            // CSMOpenFileDialog
            // 
            this.CSMOpenFileDialog.Multiselect = true;
            // 
            // chkMD5
            // 
            this.chkMD5.AutoSize = true;
            this.chkMD5.Checked = true;
            this.chkMD5.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkMD5.Location = new System.Drawing.Point(9, 19);
            this.chkMD5.Name = "chkMD5";
            this.chkMD5.Size = new System.Drawing.Size(49, 17);
            this.chkMD5.TabIndex = 5;
            this.chkMD5.Text = "MD5";
            this.chkMD5.UseVisualStyleBackColor = true;
            // 
            // chkSHA1
            // 
            this.chkSHA1.AutoSize = true;
            this.chkSHA1.Checked = true;
            this.chkSHA1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSHA1.Location = new System.Drawing.Point(64, 19);
            this.chkSHA1.Name = "chkSHA1";
            this.chkSHA1.Size = new System.Drawing.Size(57, 17);
            this.chkSHA1.TabIndex = 6;
            this.chkSHA1.Text = "SHA-1";
            this.chkSHA1.UseVisualStyleBackColor = true;
            // 
            // chkSHA256
            // 
            this.chkSHA256.AutoSize = true;
            this.chkSHA256.Checked = true;
            this.chkSHA256.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSHA256.Location = new System.Drawing.Point(127, 19);
            this.chkSHA256.Name = "chkSHA256";
            this.chkSHA256.Size = new System.Drawing.Size(69, 17);
            this.chkSHA256.TabIndex = 7;
            this.chkSHA256.Text = "SHA-256";
            this.chkSHA256.UseVisualStyleBackColor = true;
            // 
            // chkSHA384
            // 
            this.chkSHA384.AutoSize = true;
            this.chkSHA384.Checked = true;
            this.chkSHA384.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSHA384.Location = new System.Drawing.Point(202, 19);
            this.chkSHA384.Name = "chkSHA384";
            this.chkSHA384.Size = new System.Drawing.Size(69, 17);
            this.chkSHA384.TabIndex = 8;
            this.chkSHA384.Text = "SHA-384";
            this.chkSHA384.UseVisualStyleBackColor = true;
            // 
            // chkSHA512
            // 
            this.chkSHA512.AutoSize = true;
            this.chkSHA512.Checked = true;
            this.chkSHA512.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSHA512.Location = new System.Drawing.Point(277, 19);
            this.chkSHA512.Name = "chkSHA512";
            this.chkSHA512.Size = new System.Drawing.Size(69, 17);
            this.chkSHA512.TabIndex = 9;
            this.chkSHA512.Text = "SHA-512";
            this.chkSHA512.UseVisualStyleBackColor = true;
            // 
            // chkRIPEMD160
            // 
            this.chkRIPEMD160.AutoSize = true;
            this.chkRIPEMD160.Checked = true;
            this.chkRIPEMD160.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRIPEMD160.Location = new System.Drawing.Point(352, 19);
            this.chkRIPEMD160.Name = "chkRIPEMD160";
            this.chkRIPEMD160.Size = new System.Drawing.Size(86, 17);
            this.chkRIPEMD160.TabIndex = 10;
            this.chkRIPEMD160.Text = "RIPEMD160";
            this.chkRIPEMD160.UseVisualStyleBackColor = true;
            // 
            // grpbxWriteAll
            // 
            this.grpbxWriteAll.Controls.Add(this.btnWriteAll);
            this.grpbxWriteAll.Controls.Add(this.chkRIPEMD160);
            this.grpbxWriteAll.Controls.Add(this.chkMD5);
            this.grpbxWriteAll.Controls.Add(this.chkSHA512);
            this.grpbxWriteAll.Controls.Add(this.chkSHA1);
            this.grpbxWriteAll.Controls.Add(this.chkSHA384);
            this.grpbxWriteAll.Controls.Add(this.chkSHA256);
            this.grpbxWriteAll.Location = new System.Drawing.Point(12, 181);
            this.grpbxWriteAll.Name = "grpbxWriteAll";
            this.grpbxWriteAll.Size = new System.Drawing.Size(577, 46);
            this.grpbxWriteAll.TabIndex = 11;
            this.grpbxWriteAll.TabStop = false;
            this.grpbxWriteAll.Text = "Write All";
            // 
            // CSMMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(601, 510);
            this.Controls.Add(this.grpbxWriteAll);
            this.Controls.Add(this.grpbxChecksums);
            this.Controls.Add(this.lstbxFiles);
            this.Controls.Add(this.btnBrowseFiles);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CSMMainForm";
            this.Text = "Check-Sum-Mary";
            this.grpbxChecksums.ResumeLayout(false);
            this.grpbxChecksums.PerformLayout();
            this.grpbxWriteAll.ResumeLayout(false);
            this.grpbxWriteAll.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnBrowseFiles;
        private System.Windows.Forms.ListBox lstbxFiles;
        private System.Windows.Forms.ComboBox cmbHashImplementation;
        private System.Windows.Forms.Button btnWriteAll;
        private System.Windows.Forms.GroupBox grpbxChecksums;
        private System.Windows.Forms.TextBox txtMD5;
        private System.Windows.Forms.Label lblMD5;
        private System.Windows.Forms.OpenFileDialog CSMOpenFileDialog;
        private System.Windows.Forms.TextBox txtSHA256;
        private System.Windows.Forms.Label lblSHA256;
        private System.Windows.Forms.Label lblSHA1;
        private System.Windows.Forms.TextBox txtSHA1;
        private System.Windows.Forms.TextBox txtSHA512;
        private System.Windows.Forms.TextBox txtSHA384;
        private System.Windows.Forms.Label lblSHA512;
        private System.Windows.Forms.Label lblSHA384;
        private System.Windows.Forms.Label lblRIPEMD160;
        private System.Windows.Forms.TextBox txtRIPEMD160;
        private System.Windows.Forms.CheckBox chkMD5;
        private System.Windows.Forms.CheckBox chkSHA1;
        private System.Windows.Forms.CheckBox chkSHA256;
        private System.Windows.Forms.CheckBox chkSHA384;
        private System.Windows.Forms.CheckBox chkSHA512;
        private System.Windows.Forms.CheckBox chkRIPEMD160;
        private System.Windows.Forms.GroupBox grpbxWriteAll;
    }
}

