﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Security.Cryptography;

namespace Check_Sum_Mary
{
    public partial class CSMMainForm : Form
    {
        public CSMMainForm()
        {
            InitializeComponent();
        }

        public HashImplementation hashImp;

        string selectedFile;

        string sMD5;
        string sSHA1;
        string sSHA256;
        string sSHA384;
        string sSHA512;
        string sRIPEMD160;

        public enum HashFunction
        {
            MD5 = 0,
            SHA1 = 1,
            SHA256 = 2,
            SHA384 = 3,
            SHA512 = 4,
            RIPEMD160 = 5
        };

        public enum HashImplementation
        {
            Default = 0,
            CNG = 1,
            CSP = 2,
            Managed = 3
        };

        public void GetHash(Stream stream)
        {
            switch (hashImp)
            {
                case HashImplementation.Default:
                    sMD5 = GetHashString(MD5.Create().ComputeHash(stream));
                    stream.Seek(-stream.Position, SeekOrigin.Current);
                    sSHA1 = GetHashString(SHA1.Create().ComputeHash(stream));
                    stream.Seek(-stream.Position, SeekOrigin.Current);
                    sSHA256 = GetHashString(SHA256.Create().ComputeHash(stream));
                    stream.Seek(-stream.Position, SeekOrigin.Current);
                    sSHA384 = GetHashString(SHA384.Create().ComputeHash(stream));
                    stream.Seek(-stream.Position, SeekOrigin.Current);
                    sSHA512 = GetHashString(SHA512.Create().ComputeHash(stream));
                    stream.Seek(-stream.Position, SeekOrigin.Current);
                    sRIPEMD160 = GetHashString(RIPEMD160.Create().ComputeHash(stream));
                    break;
                case HashImplementation.CNG:
                    sMD5 = GetHashString(MD5Cng.Create().ComputeHash(stream));
                    stream.Seek(-stream.Position, SeekOrigin.Current);
                    sSHA1 = GetHashString(SHA1Cng.Create().ComputeHash(stream));
                    stream.Seek(-stream.Position, SeekOrigin.Current);
                    sSHA256 = GetHashString(SHA256Cng.Create().ComputeHash(stream));
                    stream.Seek(-stream.Position, SeekOrigin.Current);
                    sSHA384 = GetHashString(SHA384Cng.Create().ComputeHash(stream));
                    stream.Seek(-stream.Position, SeekOrigin.Current);
                    sSHA512 = GetHashString(SHA512Cng.Create().ComputeHash(stream));
                    stream.Seek(-stream.Position, SeekOrigin.Current);
                    sRIPEMD160 = "";
                    break;
                case HashImplementation.CSP:
                    sMD5 = GetHashString(MD5CryptoServiceProvider.Create().ComputeHash(stream));
                    stream.Seek(-stream.Position, SeekOrigin.Current);
                    sSHA1 = GetHashString(SHA1CryptoServiceProvider.Create().ComputeHash(stream));
                    stream.Seek(-stream.Position, SeekOrigin.Current);
                    sSHA256 = GetHashString(SHA256CryptoServiceProvider.Create().ComputeHash(stream));
                    stream.Seek(-stream.Position, SeekOrigin.Current);
                    sSHA384 = GetHashString(SHA384CryptoServiceProvider.Create().ComputeHash(stream));
                    stream.Seek(-stream.Position, SeekOrigin.Current);
                    sSHA512 = GetHashString(SHA512CryptoServiceProvider.Create().ComputeHash(stream));
                    stream.Seek(-stream.Position, SeekOrigin.Current);
                    sRIPEMD160 = "";
                    break;
                case HashImplementation.Managed:
                    sMD5 = "";
                    stream.Seek(-stream.Position, SeekOrigin.Current);
                    sSHA1 = GetHashString(SHA1Managed.Create().ComputeHash(stream));
                    stream.Seek(-stream.Position, SeekOrigin.Current);
                    sSHA256 = GetHashString(SHA256Managed.Create().ComputeHash(stream));
                    stream.Seek(-stream.Position, SeekOrigin.Current);
                    sSHA384 = GetHashString(SHA384Managed.Create().ComputeHash(stream));
                    stream.Seek(-stream.Position, SeekOrigin.Current);
                    sSHA512 = GetHashString(SHA512Managed.Create().ComputeHash(stream));
                    stream.Seek(-stream.Position, SeekOrigin.Current);
                    sRIPEMD160 = GetHashString(RIPEMD160Managed.Create().ComputeHash(stream));
                    break;
                default:
                    sMD5 = GetHashString(MD5.Create().ComputeHash(stream));
                    stream.Seek(-stream.Position, SeekOrigin.Current);
                    sSHA1 = GetHashString(SHA1.Create().ComputeHash(stream));
                    stream.Seek(-stream.Position, SeekOrigin.Current);
                    sSHA256 = GetHashString(SHA256.Create().ComputeHash(stream));
                    stream.Seek(-stream.Position, SeekOrigin.Current);
                    stream.Seek(-stream.Position, SeekOrigin.Current);
                    sSHA384 = GetHashString(SHA384.Create().ComputeHash(stream));
                    stream.Seek(-stream.Position, SeekOrigin.Current);
                    sSHA512 = GetHashString(SHA512.Create().ComputeHash(stream));
                    stream.Seek(-stream.Position, SeekOrigin.Current);
                    sRIPEMD160 = GetHashString(RIPEMD160.Create().ComputeHash(stream));
                    break;
            }
        }

        public string GetHashString(byte[] hBytes)
        {
            string hRet = null;

            StringBuilder sb = new StringBuilder();
            foreach (byte b in hBytes)
            {
                sb.Append(b.ToString("x2"));
            }

            hRet = sb.ToString();

            return hRet;
        }

        private void FillForm()
        {
            txtMD5.Clear();
            txtSHA1.Clear();
            txtSHA256.Clear();
            txtSHA384.Clear();
            txtSHA512.Clear();
            txtRIPEMD160.Clear();

            //if (!string.IsNullOrEmpty(sMD5))
                txtMD5.Text = sMD5;
            //if (!string.IsNullOrEmpty(sSHA1))
                txtSHA1.Text = sSHA1;
            //if (!string.IsNullOrEmpty(sSHA256))
                txtSHA256.Text = sSHA256;
            //if (!string.IsNullOrEmpty(sSHA384))
                txtSHA384.Text = sSHA384;
            //if (!string.IsNullOrEmpty(sSHA512))
                txtSHA512.Text = sSHA512;
            //if (!string.IsNullOrEmpty(sRIPEMD160))
                txtRIPEMD160.Text = sRIPEMD160;
        }


        private void btnBrowseFiles_Click(object sender, EventArgs e)
        {
            if (CSMOpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (lstbxFiles.Items.Count > 0)
                {
                    lstbxFiles.Items.Clear();
                }

                foreach (string file in CSMOpenFileDialog.FileNames)
                {
                    lstbxFiles.Items.Add(file);
                }

                if (lstbxFiles.SelectedItems.Count <= 0)
                {
                    if (lstbxFiles.Items.Count > 0)
                    {
                        selectedFile = lstbxFiles.Items[0].ToString();
                    }
                }
                else
                {
                    selectedFile = lstbxFiles.SelectedItem.ToString();
                }
            }
        }

        private void cmbHashImplementation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstbxFiles.Items.Count <= 0)
            {
                return;
            }

            string hImp = cmbHashImplementation.SelectedItem.ToString();

            switch (hImp)
            {
                case "Default":
                    hashImp = HashImplementation.Default;
                    break;
                case "CNG (Cryptography Next Generation)":
                    hashImp = HashImplementation.CNG;
                    break;
                case "CSP (Cryptographic Service Provider)":
                    hashImp = HashImplementation.CSP;
                    break;
                case "Managed":
                    hashImp = HashImplementation.Managed;
                    break;
                default:
                    hashImp = HashImplementation.Default;
                    break;
            }

            Stream s = File.OpenRead(selectedFile);

            GetHash(s);

            s.Close();
            s.Dispose();

            FillForm();
        }

        private void lstbxFiles_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstbxFiles.SelectedItems.Count <= 0)
            {
                return;
            }

            selectedFile = lstbxFiles.SelectedItem.ToString();

            Stream s = File.OpenRead(selectedFile);

            GetHash(s);

            s.Close();
            s.Dispose();

            FillForm();
        }

        private void btnWriteAll_Click(object sender, EventArgs e)
        {
            string results = null;

            Stream s;

            foreach (string file in lstbxFiles.Items)
            {
                s = File.OpenRead(file);
                GetHash(s);

                results += "File Name: " + file.Substring(file.LastIndexOf("\\") + 1) + "\r\n\r\n";

                if (chkMD5.Checked)
                {
                    results += "MD5:\r\n";
                    results += sMD5 + "\r\n\r\n";   
                }
                if (chkSHA1.Checked)                
                {
                    results += "SHA-1:\r\n";
                    results += sSHA1 + "\r\n\r\n";
                }
                if (chkSHA256.Checked)                
                {
                    results += "SHA-256:\r\n";
                    results += sSHA256 + "\r\n\r\n";
                }
                if (chkSHA384.Checked)
                {
                    results += "SHA-384:\r\n";
                    results += sSHA384 + "\r\n\r\n";
                }
                if (chkSHA512.Checked)
                    results += "SHA-512:\r\n";
                    results += sSHA512 + "\r\n\r\n";
                {
                }
                if (chkRIPEMD160.Checked)
                {
                    results += "RIPEMD160:\r\n";
                    results += sRIPEMD160 + "\r\n\r\n";
                }

                results += "===================\r\n";
                results += " END\r\n";
                results += "===================\r\n\r\n";

                s.Close();                
            }            

            ChecksumsAllResultsForm checksumsResults = new ChecksumsAllResultsForm(results);
            checksumsResults.ShowDialog();
        }
    }
}
